APP_NAME := "toybox"
CC := "tcc"
CFLAGS := "-lraylib -lGL -lm -lpthread -ldl -lrt -lX11"
CFLAGS_SHARED := "-fPIC -shared"

default: plug

build: plug main

plug:
	{{CC}} {{CFLAGS}} {{CFLAGS_SHARED}} plug.c -o libplug.so

main:
	{{CC}} {{CFLAGS}} main.c -o {{APP_NAME}}

plugpp:
	g++ {{CFLAGS}} {{CFLAGS_SHARED}} plug.cpp -o libplug.so

clean:
	rm main libplug.so

run:
	tcc -run {{CFLAGS}} main.c
