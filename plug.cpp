#include <raylib.h>
#include <iostream>

void plug_init_cpp()
{
	std::cout << "Hello, World!\n";
}

void plug_tick_cpp(float deltaTime)
{

}

void plug_frame_cpp(float deltaTime)
{
	ClearBackground(RAYWHITE);
	DrawText("This is C++", 10, 10, 20, DARKGRAY);
}

extern "C"
{
	void plug_init()
	{
		plug_init_cpp();
	} 

	void plug_tick(float deltaTime)
	{
		plug_tick_cpp(deltaTime);
	}

	void plug_frame(float frameTime)
	{
		plug_frame_cpp(frameTime);
	}
}