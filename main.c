#include <raylib.h>
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct game_plug_s 
{
	const char *filepath;
	void (*plug_init)(void);
	void (*plug_tick)(float deltaTime);
	void (*plug_frame)(float deltaTime);

	void *dlhandle;
} game_plug_t;

game_plug_t game_plug = 
{
	.filepath = "./libplug.so",
	.plug_init = 0,
	.plug_tick = 0,
	.plug_frame = 0,
	.dlhandle = 0
};

void game_plug_load()
{
	game_plug.dlhandle = dlopen(game_plug.filepath, RTLD_NOW);
	if (game_plug.dlhandle == NULL)
	{
		fprintf(stderr, "Could not open %s\n\tReason: %s\n", game_plug.filepath, dlerror());
		exit(-1);
	}

	#define FIND_SYM(x) dlsym(game_plug.dlhandle, x);

	game_plug.plug_init = FIND_SYM("plug_init");
	if(game_plug.plug_init == NULL)
	{
		fprintf(stderr, "%s\n", "Could not find plug_init");
		exit(-2);
	}

	game_plug.plug_tick = FIND_SYM("plug_tick");
	if(game_plug.plug_tick == NULL)
	{
		fprintf(stderr, "%s\n", "Could not find plug_tick");
		exit(-2);
	}

	game_plug.plug_frame = FIND_SYM("plug_frame");
	if(game_plug.plug_frame == NULL)
	{
		fprintf(stderr, "%s\n", "Could not find plug_frame");
		exit(-2);
	}
}

int main(void)
{
	const int WINDOW_WIDTH = 1024;
	const int WINDOW_HEIGHT = 576;
	const float target_fps = 60;

	game_plug_load();

	InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "ToyBox");
	SetTargetFPS(60);

	game_plug.plug_init();

	float frame_time;
	float last_tick;

	bool wasFocused = true;
	while (!WindowShouldClose()) 
	{
		bool isFocused = IsWindowFocused();
		if(isFocused && !wasFocused)
		{
			dlclose(game_plug.dlhandle);
			game_plug_load();
			game_plug.plug_init();
		}

		frame_time = GetFrameTime();
		last_tick += frame_time;
		if (last_tick > 1.0f/30.f)
		{
			game_plug.plug_tick(last_tick);
			last_tick = 0.0f;
		}
		BeginDrawing();
		game_plug.plug_frame(GetFrameTime());
		EndDrawing();

		wasFocused = isFocused;
	}
	return 0;
}
