package plug

import rl "vendor:raylib"
import "core:fmt"

@export
plug_init :: proc()
{
	fmt.printf("%s\n", "Hello, World!")
}

@export
plug_tick :: proc(delta_time: f32)
{

}

@export
plug_frame :: proc(frame_time: f32)
{
	rl.ClearBackground(rl.BLUE)
	rl.DrawText("This is Odin", 10, 10, 20, rl.WHITE)
}
